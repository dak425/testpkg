package testpkg

const Const1 = "I'm exported"
const const2 = "I'm not exported"

var Var1 = "I'm exported"
var var2 = "I'm not exported"

type StructA struct{}
type structB struct{}

func Func1() string {
	return "I'm exported"
}

func func2() string {
	return "I'm not exported"
}
